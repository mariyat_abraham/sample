﻿window.updateSortedCollection = function (records, field, value) {
		var updatedRecords = [],
			unSortedRecs = [];
		for (var i = 0; i < records.length; i++) {
			var currentRecord = records[i];
			if (currentRecord[field] == value) {
				unSortedRecs.push(currentRecord);
			} else {
				updatedRecords.push(currentRecord);
			}
		}
		updatedRecords.push.apply(updatedRecords, unSortedRecs);
		return updatedRecords;
	},
window.createSortedRecords = function (data) {
		var proxy = this, model = proxy.model,
			sortedRecords = proxy._sortedRecords,
			dataManager,
			enableAltRow = proxy.model.enableAltRow,model=this.model;
		data = proxy._spliceSummaryRows(data);
		if (!ej.isNullOrUndefined(this._selectedColumnField) && !ej.isNullOrUndefined(this._sortIgnoreValue) && model.sortSettings.sortedColumns.length > 0)
			data = proxy._updateSortedCollection(data, this._selectedColumnField, this._sortIgnoreValue);
		$.each(data, function (index, record) {
			//Skip the summary record from sorting.
			if (record.isSummaryRow) {
				var recordIndex = record.parentItem.childRecords.indexOf(record);
				record.parentItem.childRecords.splice(recordIndex, 1);
				return;
			}
			if ((proxy._filterString.length > 0 || proxy._searchString.length > 0) || (model.filterSettings.filteredColumns.length > 0)) {

				if (proxy._filteredRecords.length > 0 && proxy._filteredRecords.indexOf(record) !== -1) {

					proxy._storedIndex++;
					proxy._rowIndex++;

					if (enableAltRow) {

						record.isAltRow = proxy._rowIndex % 2 == 0 ? false : true;

					}

					sortedRecords[proxy._storedIndex] = record;

				}
			} else {

				proxy._storedIndex++;
				proxy._rowIndex++;

				if (enableAltRow) {

					record.isAltRow = proxy._rowIndex % 2 == 0 ? false : true;

				}

				sortedRecords[proxy._storedIndex] = record;

			}

			if (record.hasChildRecords && record.expanded) {
				record.childRecords = proxy._spliceSummaryRows(record.childRecords);
				dataManager = ej.DataManager(record.childRecords);
				var childRecords = dataManager.executeLocal(proxy._queryManagar).result;
				childRecords = proxy._updateSortedCollection(childRecords, "duration", 11);
				proxy._createSortedRecords(childRecords);

			}
			else if (record.hasChildRecords && !record.expanded && !model.enableVirtualization) {
				record.childRecords = proxy._spliceSummaryRows(record.childRecords);
				proxy._setChildRecords(record, sortedRecords);
			}
		});
	}
