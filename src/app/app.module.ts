import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WorkspaceComponent } from './workspace/workspace.component';
import { FormsModule } from '@angular/forms';
import { EJ_SPLITTER_COMPONENTS } from 'ej-angular2/src/ej/splitter.component';
import { EJ_TAB_COMPONENTS } from 'ej-angular2/src/ej/tab.component';
import { EJ_TREEGRID_COMPONENTS } from 'ej-angular2/src/ej/treegrid.component';
import { EJTemplateDirective } from 'ej-angular2/src/ej/template';
import { TreeGridHeaderTemplateDirective } from 'ej-angular2/src/ej/treegridtemplates/treegrid.header.template';
import { EJ_RATING_COMPONENTS } from 'ej-angular2/src/ej/rating.component';
import { EJ_TOOLTIP_COMPONENTS } from 'ej-angular2/src/ej/tooltip.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WorkspaceHelper } from './workspace/workspace.helper';

@NgModule({
  declarations: [
    AppComponent,
    WorkspaceComponent,
    EJ_SPLITTER_COMPONENTS,
    EJ_TAB_COMPONENTS,
    EJ_TREEGRID_COMPONENTS,
    EJTemplateDirective,
    TreeGridHeaderTemplateDirective,
    EJ_RATING_COMPONENTS,
    EJ_TOOLTIP_COMPONENTS,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [WorkspaceHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
