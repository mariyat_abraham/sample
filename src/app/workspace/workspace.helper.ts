import { Injectable } from '@angular/core';

@Injectable()
export class WorkspaceHelper {
    formulaHeader: any;
    attributeHeader: any;
    treeGridData: any;

    getWorkspaceData(activeIndex) {

        if (activeIndex === 1) {
        this.formulaHeader = [{ 'text': 'Formula1' },
        { 'text': 'Formula2' },
        { 'text': 'Formula3' },
        { 'text': 'Formula4' },
        { 'text': 'Formula5' }];

        this.attributeHeader = [{ 'text': 'Attribute1', 'field': 'attribute1' },
        { 'text': 'Attribute2', 'field': 'attribute2' },
        { 'text': 'Attribute3', 'field': 'attribute3' }];

        this.treeGridData = [{
            taskID: 1, taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 40,
            Formula3_id: 79, Formula4_id: 80, Formula5_id: 81,
        },
        {
            taskID: 2,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 2, Formula2_id: 41,
            Formula3_id: 82, Formula4_id: 83, Formula5_id: 84,
        },
        {
            taskID: 3,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula5: 5, Formula2_id: 42,
            Formula5_id: 87,
        },
        {
            taskID: 4,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 4, Formula2_id: 43,
            Formula4_id: 89, Formula5_id: 90,
        },
        {
            taskID: 5,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 5, Formula2_id: 44,
            Formula3_id: 91, Formula4_id: 92, Formula5_id: 93,
        },
        {
            taskID: 6,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 6, Formula2_id: 45,
            Formula3_id: 94, Formula4_id: 95, Formula5_id: 96,
        },
        {
            taskID: 27,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 7,
            Formula3_id: 97, Formula4_id: 98, Formula5_id: 99,
        },
        {
            taskID: 7,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 8, Formula2_id: 47,
            Formula3_id: 100, Formula4_id: 101, Formula5_id: 102,
        },
        {
            taskID: 8,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 9, Formula2_id: 48,
            Formula3_id: 103, Formula4_id: 104, Formula5_id: 105,
        },
        {
            taskID: 9,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 10, Formula2_id: 49,
            Formula3_id: 106, Formula4_id: 107,
        },
        {
            taskID: 10,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 11,
            Formula3_id: 109, Formula4_id: 110, Formula5_id: 111,
        },
        {
            taskID: 11,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 51,
            Formula3_id: 112, Formula4_id: 113, Formula5_id: 114,
        },
        {
            taskID: 12,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula5: 5, Formula1_id: 13,
            Formula3_id: 115, Formula5_id: 117,
        },
        {
            taskID: 13,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula5: 5, Formula1_id: 14, Formula2_id: 53,
            Formula3_id: 118, Formula5_id: 120,
        },
        {
            taskID: 14,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 15, Formula2_id: 54,
            Formula3_id: 121, Formula4_id: 122, Formula5_id: 123,
        },
        {
            taskID: 15,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 16, Formula2_id: 55,
            Formula3_id: 124, Formula4_id: 125, Formula5_id: 126,

        },
        {
            taskID: 16,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 56,
            Formula3_id: 127, Formula4_id: 128, Formula5_id: 129,
        },
        {
            taskID: 17,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 18, Formula2_id: 57,
            Formula3_id: 130, Formula4_id: 131, Formula5_id: 132,
        },
        {
            taskID: 18,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 19, Formula2_id: 58,
            Formula3_id: 134, Formula4_id: 135,
        },
        {
            taskID: 19,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 20, Formula2_id: 59,
            Formula3_id: 137, Formula4_id: 138, Formula5_id: 139,
        },
        {
            taskID: 20,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 21, Formula2_id: 60,
            Formula4_id: 141, Formula5_id: 142,
        },
        {
            taskID: 21,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 22, Formula2_id: 61,
            Formula3_id: 143, Formula4_id: 144, Formula5_id: 145,
        },
        {
            taskID: 22,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula5: 5, Formula1_id: 23, Formula2_id: 62,
            Formula3_id: 146, Formula5_id: 148,
        },
        {
            taskID: 23,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula5: 5, Formula1_id: 24, Formula2_id: 63,
            Formula3_id: 149, Formula5_id: 151,
        },
        {
            taskID: 24,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 25, Formula2_id: 64,
            Formula3_id: 152, Formula4_id: 153, Formula5_id: 154,
            subtasks: [
                {
                    duration1: 10, taskID: 28, taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 26,
                    Formula2_id: 65, Formula3_id: 155, Formula4_id: 156, Formula5_id: 157,
                },
                {
                    duration1: 10, taskID: 29, taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 27,
                    Formula2_id: 66, Formula3_id: 158, Formula4_id: 159, Formula5_id: 160,
                },
                {
                    duration1: 10, taskID: 30, taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 28,
                    Formula2_id: 67, Formula3_id: 161, Formula4_id: 162, Formula5_id: 163,
                },
                {
                    duration1: 10, taskID: 31, taskName: 'Planning complete', startDate: '02/07/2014', endDate: '02/07/2014',
                    duration: 0, progress: 0, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 29,
                    Formula2_id: 68, Formula3_id: 164, Formula4_id: 165, Formula5_id: 166,
                }
            ]
        },
        {
            taskID: 25,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 30, Formula2_id: 69,
            Formula3_id: 167, Formula4_id: 168, Formula5_id: 169,
            subtasks: [
                {
                    duration1: 10, taskID: 32, taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 31,
                    Formula2_id: 70, Formula3_id: 170, Formula4_id: 171, Formula5_id: 172,
                },
                {
                    duration1: 10, taskID: 33, taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 32,
                    Formula2_id: 71, Formula3_id: 173, Formula4_id: 174, Formula5_id: 175,
                },
                {
                    duration1: 10, taskID: 34, taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 33,
                    Formula2_id: 72, Formula3_id: 176, Formula4_id: 177, Formula5_id: 178,
                },
                {
                    duration1: 10, taskID: 35, taskName: 'Planning complete', startDate: '02/07/2014', endDate: '02/07/2014',
                    duration: 0, progress: 0, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 34,
                    Formula2_id: 73, Formula3_id: 179, Formula4_id: 180, Formula5_id: 181,
                }
            ]
        },
        {
            taskID: 26,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 35, Formula2_id: 74,
            Formula3_id: 182, Formula4_id: 183, Formula5_id: 184,
            subtasks: [
                {
                    duration1: 10, taskID: 36, taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 36,
                    Formula2_id: 75, Formula3_id: 185, Formula4_id: 186, Formula5_id: 187,
                },
                {
                    duration1: 10, taskID: 37, taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 37,
                    Formula2_id: 76, Formula3_id: 188, Formula4_id: 189, Formula5_id: 190,
                },
                {
                    duration1: 10, taskID: 38, taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014',
                    duration: 5, progress: 100, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 38,
                    Formula2_id: 77, Formula3_id: 191, Formula4_id: 192, Formula5_id: 193,
                },
                {
                    duration1: 10, taskID: 39, taskName: 'Planning complete', startDate: '02/07/2014', endDate: '02/07/2014',
                    duration: 0, progress: 0, Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 39,
                    Formula2_id: 78, Formula3_id: 194, Formula4_id: 195, Formula5_id: 196,
                }
            ],
        }];
    }

    if (activeIndex === 2) {
        this.formulaHeader = [{ 'text': 'Formula1' },
        { 'text': 'Formula2' },
        { 'text': 'Formula3' },
        { 'text': 'Formula4' },
        { 'text': 'Formula5' }];

        this.attributeHeader = [{ 'text': 'Attribute1', 'field': 'attribute1' },
        { 'text': 'Attribute2', 'field': 'attribute2' },
        { 'text': 'Attribute3', 'field': 'attribute3' }];

        this.treeGridData = [{
            taskID: 1, taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 40,
            Formula3_id: 79, Formula4_id: 80, Formula5_id: 81,
        },
        {
            taskID: 2,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 2, Formula2_id: 41,
            Formula3_id: 82, Formula4_id: 83, Formula5_id: 84,
        },
        {
            taskID: 3,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula5: 5, Formula2_id: 42,
            Formula5_id: 87,
        },
        {
            taskID: 4,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 4, Formula2_id: 43,
            Formula4_id: 89, Formula5_id: 90,
        },
        {
            taskID: 5,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 5, Formula2_id: 44,
            Formula3_id: 91, Formula4_id: 92, Formula5_id: 93,
        },
        {
            taskID: 6,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 6, Formula2_id: 45,
            Formula3_id: 94, Formula4_id: 95, Formula5_id: 96,
        },
        {
            taskID: 27,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 7,
            Formula3_id: 97, Formula4_id: 98, Formula5_id: 99,
        },
        {
            taskID: 7,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 8, Formula2_id: 47,
            Formula3_id: 100, Formula4_id: 101, Formula5_id: 102,
        },
        {
            taskID: 8,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 9, Formula2_id: 48,
            Formula3_id: 103, Formula4_id: 104, Formula5_id: 105,
        },
        {
            taskID: 9,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 10, Formula2_id: 49,
            Formula3_id: 106, Formula4_id: 107,
        },
        {
            taskID: 10,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 11,
            Formula3_id: 109, Formula4_id: 110, Formula5_id: 111,
        },
    ];
    }

    if (activeIndex === 3) {
        this.formulaHeader = [{ 'text': 'Formula1' },
        { 'text': 'Formula2' },
        { 'text': 'Formula3' },
        { 'text': 'Formula4' },
        { 'text': 'Formula5' },
        { 'text': 'Formula6' },
        { 'text': 'Formula7' },
        { 'text': 'Formula8' }];

        this.attributeHeader = [{ 'text': 'Attribute1', 'field': 'attribute1' },
        { 'text': 'Attribute2', 'field': 'attribute2' },
        { 'text': 'Attribute3', 'field': 'attribute3' }];

        this.treeGridData = [{
            taskID: 1, taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 40,
            Formula3_id: 79, Formula4_id: 80, Formula5_id: 81,
        },
        {
            taskID: 2,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 2, Formula2_id: 41,
            Formula3_id: 82, Formula4_id: 83, Formula5_id: 84,
        },
        {
            taskID: 3,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula5: 5, Formula2_id: 42,
            Formula5_id: 87,
        },
        {
            taskID: 4,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 4, Formula2_id: 43,
            Formula4_id: 89, Formula5_id: 90,
        },
        {
            taskID: 5,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 5, Formula2_id: 44,
            Formula3_id: 91, Formula4_id: 92, Formula5_id: 93,
        },
        {
            taskID: 6,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 6, Formula2_id: 45,
            Formula3_id: 94, Formula4_id: 95, Formula5_id: 96,
        },
        {
            taskID: 27,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 7,
            Formula3_id: 97, Formula4_id: 98, Formula5_id: 99,
        },
        {
            taskID: 7,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 8, Formula2_id: 47,
            Formula3_id: 100, Formula4_id: 101, Formula5_id: 102,
        },
        {
            taskID: 8,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 9, Formula2_id: 48,
            Formula3_id: 103, Formula4_id: 104, Formula5_id: 105,
        },
        {
            taskID: 9,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 10, Formula2_id: 49,
            Formula3_id: 106, Formula4_id: 107,
        },
        {
            taskID: 10,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 11,
            Formula3_id: 109, Formula4_id: 110, Formula5_id: 111,
        },
    ];
    }

    if (activeIndex === 4) {
        this.formulaHeader = [{ 'text': 'Formula1' },
        { 'text': 'Formula2' },
        { 'text': 'Formula3' },
        { 'text': 'Formula4' },
        { 'text': 'Formula5' },
        { 'text': 'Formula6' },
        { 'text': 'Formula7' },
        { 'text': 'Formula8' },
        { 'text': 'Formula9' },
        { 'text': 'Formula10' },
    ];

        this.attributeHeader = [{ 'text': 'Attribute1', 'field': 'attribute1' },
        { 'text': 'Attribute2', 'field': 'attribute2' },
        { 'text': 'Attribute3', 'field': 'attribute3' }];

        this.treeGridData = [{
            taskID: 1, taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 40,
            Formula3_id: 79, Formula4_id: 80, Formula5_id: 81,
        },
        {
            taskID: 2,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 2, Formula2_id: 41,
            Formula3_id: 82, Formula4_id: 83, Formula5_id: 84,
        },
        {
            taskID: 3,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula5: 5, Formula2_id: 42,
            Formula5_id: 87,
        },
        {
            taskID: 4,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 4, Formula2_id: 43,
            Formula4_id: 89, Formula5_id: 90,
        },
        {
            taskID: 5,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 5, Formula2_id: 44,
            Formula3_id: 91, Formula4_id: 92, Formula5_id: 93,
        },
        {
            taskID: 6,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 6, Formula2_id: 45,
            Formula3_id: 94, Formula4_id: 95, Formula5_id: 96,
        },
        {
            taskID: 27,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 7,
            Formula3_id: 97, Formula4_id: 98, Formula5_id: 99,
        },
        {
            taskID: 7,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 8, Formula2_id: 47,
            Formula3_id: 100, Formula4_id: 101, Formula5_id: 102,
        },
        {
            taskID: 8,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 9, Formula2_id: 48,
            Formula3_id: 103, Formula4_id: 104, Formula5_id: 105,
        },
        {
            taskID: 9,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 10, Formula2_id: 49,
            Formula3_id: 106, Formula4_id: 107,
        },
        {
            taskID: 10,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 11,
            Formula3_id: 109, Formula4_id: 110, Formula5_id: 111,
        },
        {
            taskID: 11,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 51,
            Formula3_id: 112, Formula4_id: 113, Formula5_id: 114,
        },
        {
            taskID: 12,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula3: 2, Formula5: 5, Formula1_id: 13,
            Formula3_id: 115, Formula5_id: 117,
        },
        {
            taskID: 13,
            taskName: 'Planning complete', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula5: 5, Formula1_id: 14, Formula2_id: 53,
            Formula3_id: 118, Formula5_id: 120,
        },
        {
            taskID: 14,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 15, Formula2_id: 54,
            Formula3_id: 121, Formula4_id: 122, Formula5_id: 123,
        },
        {
            taskID: 15,
            taskName: 'Plan timeline', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 16, Formula2_id: 55,
            Formula3_id: 124, Formula4_id: 125, Formula5_id: 126,

        },
        {
            taskID: 16,
            taskName: 'Allocate resources', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula2_id: 56,
            Formula3_id: 127, Formula4_id: 128, Formula5_id: 129,
        },
        {
            taskID: 17,
            taskName: 'Plan budget', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 18, Formula2_id: 57,
            Formula3_id: 130, Formula4_id: 131, Formula5_id: 132,
        },
        {
            taskID: 18,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula3: 2, Formula4: 3, Formula1_id: 19, Formula2_id: 58,
            Formula3_id: 134, Formula4_id: 135,
        },
        {
            taskID: 19,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula2: 1, Formula3: 2, Formula4: 3, Formula5: 5, Formula1_id: 20, Formula2_id: 59,
            Formula3_id: 137, Formula4_id: 138, Formula5_id: 139,
        },
        {
            taskID: 20,
            taskName: 'Planning', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
            Formula1: 0, Formula2: 1, Formula4: 3, Formula5: 5, Formula1_id: 21, Formula2_id: 60,
            Formula4_id: 141, Formula5_id: 142,
        },
    ];
    }
    const data = [this.formulaHeader, this.attributeHeader, this.treeGridData];
    return data;
}
}
