import {
    Component,
    ViewChild,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    Input, SimpleChanges,
    OnChanges,
    SimpleChange
} from '@angular/core';
import { EJComponents } from 'ej-angular2/src/ej/core';
import findIndex from 'lodash/findIndex';
import cloneDeep from 'lodash/cloneDeep';
import { WorkspaceHelper } from './workspace.helper';
import { colorConfig } from './color-config';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class WorkspaceComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('FormulaTreeGridControl') FormulaTreeGridControl: EJComponents<any, any>;
  @ViewChild('AttributeTreeGridControl') AttributeTreeGridControl: EJComponents<any, any>;
    @Input() activeIndex: any;
  public treeGridData: any;
  public treeGridData1: any;
  public formulaHeader: any;
  public attributeHeader: any;
  public gridHeight: number;
  public orientation: any;
  public columnResizeSettings: any;
  public splitterProperties: any;
  public treeColumnIndex: number;
  public selection: any;
  private formulaEditParams = {};
  public decimalFormat: any = {};
  public editSettings: any;
  // tslint:disable-next-line:no-inferrable-types
  private frozenColumLength: number = -1;
  private selectedColIndex = -1;
  public dragTooltip: any = {};
  public formulaTreeGridWidget: any;
  public attributeTreeGridWidget: any;
  public decimalDigits: number;
  public formulaFormat: any;
  public contextMenuSettings: { showContextMenu: boolean; contextMenuItems: any[]; };
  // tslint:disable-next-line:no-inferrable-types
  public isEndEditCalled: boolean = false;
  public selectedRowIndex: any;
  // tslint:disable-next-line:no-inferrable-types
  public isSubformulaSelected: boolean = false;
  public columnDrag: any = [];
  public isCheckboxSelection: boolean;
  public selectedColumnFieldName;
  public previousCellValue: any;
  public gridData: any;
  public gridSelectedRows: any = [];
  public dataSourceFlagFormula: boolean;
  public columnUpdateFlagFormula: boolean;
  public allowRowDragDrop: boolean;
  public data: any;
  public decimalValueLength = 3;
    public checkboxState = { checked: 'checked', unchecked: 'unchecked' };
    public dataSourceFlagAttr: boolean;
    summaryRows: any;
    summaryRowsForAttributeGrid: any;
    public colorList = colorConfig.colorList;
    public columnUpdateFlagAttribute: any;

    constructor(private workspaceHelper: WorkspaceHelper) {
    this.treeColumnIndex = 2;
    this.dragTooltip = {
      showTooltip: true,
    };
    this.columnResizeSettings = this.getColumnResizeSettings();
    this.selection = this.getSelectionSettings();
    this.formulaFormat = this.getFormulaFormat(3);
    this.editSettings = this.getEditSettings();
    this.orientation = ej.Orientation.Horizontal;
    this.splitterProperties = [
      {
        paneSize: '70%',
        collapsible: false,
        expandable: false
      },
      {
        paneSize: '30%',
        collapsible: true,
        expandable: false
      }
    ];
    this.decimalDigits = 3;
    this.contextMenuSettings = {
      showContextMenu: true,
      contextMenuItems: []
    };
  }

    ngOnInit() {
        this.gridHeight = window.innerHeight - 78;
        this.allowRowDragDrop = false;
        this.showSummary();
    }

    ngOnChanges(changes: SimpleChanges) {
        const activeIndex: SimpleChange = changes.activeIndex;
        if (activeIndex && activeIndex.currentValue !== undefined) {
            this.selectedColIndex = -1;
            this.formulaHeader = [];
            this.attributeHeader = [];
            this.treeGridData = [];
            this.data = this.workspaceHelper.getWorkspaceData(activeIndex.currentValue);
            this.formulaHeader = this.data[0];
            this.attributeHeader = this.data[1];
            setTimeout(() => {
                this.treeGridData = this.data[2];
                this.dataSourceFlagFormula = true;
                this.dataSourceFlagAttr = true;
                this.columnUpdateFlagFormula = true;
                this.columnUpdateFlagAttribute = true;
                this.allowRowDragDrop = false;
                if (this.selectedColIndex === -1) {
                    this.disableCheckboxes();
                }
            }, );
        }

    }

  ngAfterViewInit() {
    if (this.formulaHeader.length < 5) {
      this.updateSpliterPositionOnColumnResize();
    } else {
      this.updateSplitterMinSize();
    }
    this.resizeTreeGridWindow();
    const obj = $('#outterSpliter').ejSplitter('instance');
    obj.refresh();
    this.onColumnSelection(this);
  }

  getColumnResizeSettings() {
    return {
      columnResizeMode: ej.TreeGrid.ColumnResizeMode.FixedColumns
    };
  }

  getSelectionSettings() {
    return {
      selectionType: ej.TreeGrid.SelectionType.Checkbox,
      selectionMode: ej.TreeGrid.SelectionMode.Row,
      enableSelectAll: true,
      enableHierarchySelection: false
    };
  }
  getFormulaFormat(decimalValue: number): string {
    const decimalLength = '0:N' + decimalValue;
    return this.formulaFormat = '{' + decimalLength + '}';
  }

  getEditSettings() {
    return {
      allowResizeToFit: true,
      allowAdding: true,
      allowEditing: true,
      allowDeleting: true,
      beginEditAction: ej.TreeGrid.BeginEditAction.Click,
      editMode: 'cellEditing',
      rowPosition: 'belowSelectedRow'
    };
  }
  splitterCreate(args) {
    this.resizeTreeGridWindow();
  }

  splitterResize() {
    this.resizeTreeGridWindow();
    this.updateScrollMatch();
  }

  splitterExpandOrCollapse(args) {
    this.resizeTreeGridWindow();
  }

  resizeTreeGridWindow() {
    const formulaTreeObj = this.getFormulaTreeGridObject();
    formulaTreeObj._windowResize();
    const attributeTreeObj = this.getAttributeTreeGridObject();
    attributeTreeObj._windowResize();
  }

  updateScrollMatch() {
    const forumulaObj = $('#FormulaTreeGridControl').ejTreeGrid('instance'),
      formulaScroller = forumulaObj.getScrollElement(),
      attrObj = $('#AttributeTreeGridControl').ejTreeGrid('instance'),
      attrScroller = attrObj.getScrollElement(),
      leftScroll = formulaScroller.ejScroller('isHScroll'),
      rightScroll = attrScroller.ejScroller('isHScroll');
    if (leftScroll !== rightScroll) {
      if (leftScroll && !$('#AttributeTreeGridControl-footersummaryrow').hasClass('e-temp-scroll-div')) {
        $('#AttributeTreeGridControl-footersummaryrow').addClass('e-temp-scroll-div');
        attrObj._windowResize();
      } else if (!leftScroll && $('#AttributeTreeGridControl-footersummaryrow').hasClass('e-temp-scroll-div')) {
        $('#AttributeTreeGridControl-footersummaryrow').removeClass('e-temp-scroll-div');
        attrObj._windowResize();
      }
    } else if ($('#AttributeTreeGridControl-footersummaryrow').hasClass('e-temp-scroll-div')) {
      $('#AttributeTreeGridControl-footersummaryrow').removeClass('e-temp-scroll-div');
      attrObj._windowResize();
    }
  }

  private getFormulaTreeGridObject() {
    return $('#FormulaTreeGridControl').ejTreeGrid('instance');
  }
  private getAttributeTreeGridObject() {
    return $('#AttributeTreeGridControl').ejTreeGrid('instance');
  }

  load(args) {
    ej.TreeGrid.Locale['en-US'].emptyRecord = `<i class="fa fa-info-circle" style="font-size:25px;padding-top:6px;padding-left:10px;"></i>
        <span style='color:#337dce;padding-left:10px;'>Press INSERT to add materials to your formula</span>`;
    const formulaTreeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    formulaTreeObj.treeIndentLevelWidth = 12;
  }

  loadAttributeGrid(args) {
    ej.TreeGrid.Locale['en-US'].emptyRecord = ``;
  }
  createFormulaTreeGrid(args) {
    if (this.selectedColIndex === -1) {
      $('#FormulaTreeGridControl').find('.e-gridheader .e-chkbox-small').addClass('e-disable').css('pointer-events', 'none');
    }
  }

  public formulaGridRowSelecting(args) {

    const selectedIngredient = args.data;
    if (args.isCheckboxUpdate === true) {// If checkbox is selected
      const workspace = selectedIngredient[this.selectedColumnFieldName];
      if (workspace === undefined || workspace === '-xxx-' || args.data.level > 0) {
        args.cancel = true;
      }
    }
    const formulaTreeGridObj = this.getFormulaTreeGridObject();
    if (formulaTreeGridObj._isOnContextMenuAction) {
      formulaTreeGridObj._isOnContextMenuAction = false;
      args.cancel = true;
    }
  }

  private formulaCellInfo(event) {
    this.colorDifferentSFLevels(event);
    const formulaEditParams = this.getFormulaEditParams(3, false, true);
    event.column.editParams = formulaEditParams;
    const decimalLength = 3;
    this.decimalFormat['digitsInfo'] = 1 + '.' + decimalLength + '-' + decimalLength;
    this.decimalFormat['locale'] = 'en-US';
    const formulaTreeObj = this.getFormulaTreeGridObject();
    this.setBackground(formulaTreeObj, formulaTreeObj._selectedColumnIndex);
    if (event.cellValue === undefined && event.column.headerText !== '' || event.cellValue === -1) {
      $(event.cellElement).text('-xxx-');
    }
    if (event.column.field === 'checkboxState') {
      if (this.selectedColIndex === -1 || event.data.level > 0) {
        $(event.cellElement).find('.e-chkbox-small').addClass('e-disable').css('pointer-events', 'none');
      }
    }

    if (event.data.progress === 100) {
      $(event.cellElement).addClass('restricted-ingredient');
    }

    if (event.column.headerText === 'Task Name' && event.data.item.subtasks === undefined && event.data.level === 0) {
      $(event.cellElement).addClass('revert-indent');
    }

    this.setBackground(formulaTreeObj, formulaTreeObj._selectedColumnIndex);
    if (event.column.field === 'checkboxState') {
      if (event.data.level > 0) {
        $(event.cellElement).find('.e-chkbox-small').addClass('e-disable').css('pointer-events', 'none');
      }
    }
  }

  attributeCellInfo(event) {
    this.colorDifferentSFLevels(event);
  }

  private onFormulaBeginEdit(event) {
    const formulaIndex = event.columnIndex - this.frozenColumLength;
    this.previousCellValue = event.cellElement[0].innerText;
    if (event.data.level > 0) {
      event.cancel = true;
    }
    if (!(formulaIndex === this.selectedColIndex)) {
      this.disableFormulaEditing();
      return;
    }
  }

  private onFormulaEndEdit(event) {
        const formulaTreeObj = this.getFormulaTreeGridObject();
        const attributeObj = this.getAttributeTreeGridObject();
    this.isEndEditCalled = true;
    if ((this.previousCellValue === '-xxx-' && event.value === 0) || (event.value === null) ||
      (event.value === null && event.previousValue === null)) {
      event.cellElement.text('-xxx-');
      event.data[event.columnName] = event.data.item[event.columnName] = '-xxx-';
    } else if (this.previousCellValue === '-xxx-' && event.value !== 0 && event.value !== null) {
      for (let i = 0; i < this.treeGridData.length; i++) {
        const idx = findIndex(this.treeGridData, { taskID: event.data.taskID });
        this.treeGridData[idx][event.columnName + '_id'] = 300;
      }
      this.gridData = this.treeGridData;
        }
        if (event.data.checkboxState === this.checkboxState.unchecked) {
            formulaTreeObj.selectRows(this.selectedRowIndex, null, true);
            attributeObj.selectRows(this.selectedRowIndex, null, true);
    }
  }

  private disableFormulaEditing() {
    this.editSettings = {
      allowResizeToFit: false,
      allowAdding: false,
      allowEditing: false,
      allowDeleting: false,
      beginEditAction: ej.TreeGrid.BeginEditAction.Click,
      editMode: 'cellEditing',
      rowPosition: 'belowSelectedRow'
    };
    setTimeout(() => {
      this.editSettings = this.getEditSettings();
    }, 0);
  }
  getFormulaEditParams(decimalPlaces: number, showSpinButton: boolean, showRoundedCorner: boolean) {
    return this.formulaEditParams = {
      decimalPlaces: decimalPlaces,
      showSpinButton: false,
      showRoundedCorner: true,
      create: function (args) {
        this.element.off('keydown');
        if (ej.isNullOrUndefined(this.model.value) || this.model.value === -1) {
          this.option('value', 0.0);
        }
      },
      minValue: 0
    };
  }

    formulaGridActionBegin(args) {
        if (args.requestType === 'delete') {
            args.cancel = true;
          }
          if (args.requestType === 'refreshDataSource') {
            if (this.dataSourceFlagFormula) {
              if (this.columnUpdateFlagFormula) {
                args.model.columns = this.updateFormulaColumns();
                args.isUpdateColumns = true;
              }
            } else {
              args.cancel = true;
            }
          }
    }

    attrGridActionBegin(args) {
        if (args.requestType === 'refreshDataSource') {
            if (this.dataSourceFlagAttr) {
              if (this.columnUpdateFlagAttribute) {
                args.model.columns = this.updateAttributeColumns();
                args.isUpdateColumns = true;
              }
            } else {
              args.cancel = true;
            }
          }
    }

  private updateFormulaColumns() {
    const newCols = {};
    const element = 'columns', item = (<any>this.FormulaTreeGridControl)['tag_columns'];

    // Read updated columns in DOM
    item.list = item.children.map((child) => {
      child.property = item.propertyName;
      return child;
    });

    // Update column properties in list
    if (!ej.isNullOrUndefined(item)) {
      ej.createObject(element, item.getList(), newCols);
    }
    const treegridObj = this.getFormulaTreeGridObject();
    treegridObj.model.treeColumnIndex = 2;
    return newCols['columns'];
  }

  private updateAttributeColumns() {
    const newCols = {};
    const element = 'columns';
    try {
      const item = (<any>this.AttributeTreeGridControl)['tag_columns'];
      // Read updated columns in DOM
      item.list = item.children.map((child) => {
        child.property = item.propertyName;
        return child;
      });
      // Update column properties in list
      if (!ej.isNullOrUndefined(item)) {
        ej.createObject(element, item.getList(), newCols);
      }
      return newCols['columns'];
    } catch (error) {
    }
  }

  formulaGridActionComplete(args) {
    if (args.requestType === 'scroll') {
        const attributeTreeObj = this.getAttributeTreeGridObject();
        let treeObj;
        this.setBackground(attributeTreeObj, attributeTreeObj._selectedColumnIndex);
        if ($('#FormulaTreeGridControl').hasClass('e-treegrid')) {
          treeObj = this.getFormulaTreeGridObject();
          this.setScrollTop(attributeTreeObj._scrollTop, treeObj);
        }
      } else if (args.requestType === 'sorting') {
      const treeGridObj = this.getAttributeTreeGridObject();
      if (args.model.sortSettings.sortedColumns.length > 0
        && JSON.stringify(args.model.sortSettings.sortedColumns) !== JSON.stringify(treeGridObj.model.sortSettings.sortedColumns)) {
        const sortSettings = {
          sortedColumns: $.extend([], args.model.sortSettings.sortedColumns)
        };
        treeGridObj.setModel({ 'sortSettings': sortSettings });
      } else if (args.model.sortSettings.sortedColumns.length === 0 && treeGridObj.model.sortSettings.sortedColumns.length !== 0) {
        treeGridObj.clearSorting();
      }
      this.addColumnSelectionClasses(this.getFormulaTreeGridObject(), this.selectedColumnFieldName, true);
    } else if (args.requestType === 'refreshDataSource') {
      this.dataSourceFlagFormula = false;
      this.columnUpdateFlagFormula = false;
    }

  }

  attrGridActionComplete(args) {
    if (args.requestType === 'scroll') {
        const attributeTreeObj = this.getAttributeTreeGridObject();
        let treeObj;
        this.setBackground(attributeTreeObj, attributeTreeObj._selectedColumnIndex);
        if ($('#FormulaTreeGridControl').hasClass('e-treegrid')) {
          treeObj = this.getFormulaTreeGridObject();
          this.setScrollTop(attributeTreeObj._scrollTop, treeObj);
        }
      } else if (args.requestType === 'sorting') {
      const treeGridObj = this.getFormulaTreeGridObject();
      if (args.model.sortSettings.sortedColumns.length > 0
        && JSON.stringify(args.model.sortSettings.sortedColumns) !== JSON.stringify(treeGridObj.model.sortSettings.sortedColumns)) {
        const sortSettings = {
          sortedColumns: $.extend([], args.model.sortSettings.sortedColumns)
        };
        treeGridObj.setModel({ 'sortSettings': sortSettings });
      } else if (args.model.sortSettings.sortedColumns.length === 0 && treeGridObj.model.sortSettings.sortedColumns.length !== 0) {
        treeGridObj.clearSorting();
      }
    }
  }

  setScrollTop(scrolltop, treeObj1) {
    // tslint:disable-next-line:radix
    if (parseInt(treeObj1._scrollTop) !== parseInt(scrolltop)) {
      treeObj1.scrollOffset(null, scrolltop);
    }
  }

  public formulaGridRowSelected(args) {
    const formulaTreeGridWidget = this.getFormulaTreeGridObject();
    let idx = findIndex(formulaTreeGridWidget.model.updatedRecords, { taskID: args.data.taskID });
    idx = (idx === -1) ? formulaTreeGridWidget.model.updatedRecords.length : idx;
    this.setSelectedRowIndex(idx);

    if (args.data.item.subtasks !== undefined && args.data.expanded) {
      this.isSubformulaSelected = true;
    } else {
      this.isSubformulaSelected = false;
    }
    const attributeTreeObj = this.getAttributeTreeGridObject();
    attributeTreeObj.selectRows(args.recordIndex);
    const formula = this.selectedColumnFieldName;
    const formulaId = formula + '_id';
    const alreadySelectedIngredient = this.gridSelectedRows.length ?
      findIndex(this.gridSelectedRows, { [formulaId]: args.data[formulaId] }) : -1;
    if (formula !== undefined || formula !== null) {
      const formulaIngredients = this.gridData;
      if (args.isCheckboxUpdate === true) { // If checkbox row selection
        if (this.isCheckboxSelection === false) {
          // Clear row selection by click/ enter / up & down arrow
          if (alreadySelectedIngredient === -1) {
            this.deselectSelectedRows();
          }
          this.gridSelectedRows = [];
        }
        this.isCheckboxSelection = true;
        // If already selected, then remove it from array
        if (alreadySelectedIngredient !== -1) {
          this.gridSelectedRows.splice(alreadySelectedIngredient, 1);
        } else {
          // If checkbox is selected
          const selectedIngredient = args.data.item;
          this.gridSelectedRows.push(selectedIngredient);
          formulaTreeGridWidget.selectRows(args.recordIndex);
        }
      } else {
        // restrict row selection for subformula items
        if (args.data.level > 0) {
          formulaTreeGridWidget.selectRows(args.recordIndex, null, false);
        } else {
          // restrict checkbox selection for unrelated ingredients
          const index = findIndex(formulaIngredients, { [formulaId]: args.data.item[formulaId] });
          if (index === -1) {
            formulaTreeGridWidget.cellEdit(args.recordIndex, this.selectedColumnFieldName);
            formulaTreeGridWidget.selectRows(args.recordIndex, null, false);
          } else {
            // Normal row selection by clicking on row or pressing enter key/ up, down arrow
            this.isCheckboxSelection = false;
            // Deselect previous checkbox
            this.deselectSelectedRows();
            // Clear the selectedIngredients array and push new selected item
            this.gridSelectedRows = [];
            // Select the current row with checkbox enabled
            const selectedIngredient = args.data.item;
            const workspace = selectedIngredient[this.selectedColumnFieldName];
            if (workspace !== undefined) {
                            formulaTreeGridWidget.selectRows(args.recordIndex, null, true);
                            attributeTreeObj.selectRows(args.recordIndex);
              this.gridSelectedRows.push(selectedIngredient);
              formulaTreeGridWidget.selectRows(args.recordIndex, null, true);
              formulaTreeGridWidget.cellEdit(args.recordIndex, this.selectedColumnFieldName);
            }
          }
        }
      }
      if (formulaIngredients.length === this.gridSelectedRows.length) {
        $('#FormulaTreeGridControl_headerCheckbox')
          .addClass('e-checkbox e-chk-image e-icon e-headerCheckbox .e-chkbox-wrap .e-chk-image.e-checkmark e-checkmark');
      }
    }
  }

  public deselectSelectedRows() {
    const treeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    const experimentId = this.selectedColumnFieldName;
    const formulaId = this.selectedColumnFieldName + '_id';
    for (let i = 0; i < this.gridSelectedRows.length; i++) {
      let ingredientIndex = findIndex(treeObj.model.updatedRecords,
        { [formulaId]: this.gridSelectedRows[i][formulaId] });
      if (ingredientIndex === -1) {
        ingredientIndex = findIndex(treeObj.model.flatRecords, { [formulaId]: this.gridSelectedRows[i][formulaId] });
      }
      if (ingredientIndex !== -1) {
        treeObj.selectRows(ingredientIndex, null, true);
      }
    }
  }

  public attributeGridRowSelected(args) {
    const formulaTreeObj = this.getFormulaTreeGridObject();
    formulaTreeObj.selectRows(args.recordIndex);
  }

  updateSpliterPositionOnColumnResize() {
    const treeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance'),
      splitterObj = $('#outterSpliter').ejSplitter('instance'),
      frozenWidth = $(treeObj.getRows()[0]).width(),
      leftTreeGridWidth = $(treeObj.getRows()[1]).width() + frozenWidth + 5,
      rightTreeGridWidth = $('#outterSpliter').width() - leftTreeGridWidth - 10,
      properties = [{ paneSize: leftTreeGridWidth + 'px', collapsible: false, minSize: (frozenWidth + 22) + 'px' },
      { paneSize: rightTreeGridWidth + 'px' }];
    splitterObj.option('properties', properties);
    this.splitterResize();
  }

  updateSplitterMinSize() {
    const formulaTreeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance'),
      splitterObj = $('#outterSpliter').ejSplitter('instance');
    if (formulaTreeObj !== undefined) {
      const width = ($(formulaTreeObj.getRows()[0]).width() + 22) + 'px',
        properties = [{ paneSize: '70%', collapsible: false, minSize: width, expandable: false },
        { paneSize: '30%', collapsible: true, expandable: false }];
      splitterObj.option('properties', properties);
    }
    this.updateScrollMatch();
  }
  public getIngredientReferenceIcon(): string {
    const imgPath = 'assets/Check' + '.jpg';
    return imgPath;
  }

    public subFormulaExpandOrCollapseEventClicked(object) {
        this.attributeTreeGridWidget = this.AttributeTreeGridControl.widget;
        this.attributeTreeGridWidget.expandCollapseRow(object.recordIndex);
        if (this.selectedColumnFieldName !== undefined) {
            this.setRowFocus(object);
        }
    }

    public setRowFocus(subFormulaObject) {
        this.clearCheckboxSelection();
        const formulaTreeGrid = $('#FormulaTreeGridControl').ejTreeGrid('instance');
        const attributeTreeGrid = $('#AttributeTreeGridControl').ejTreeGrid('instance');
        const selectedIngredient = subFormulaObject.data.item;
        const formula = this.selectedColumnFieldName;
        const workspace = selectedIngredient[subFormulaObject.data.item[formula]];
        if (workspace !== undefined) {
            if (subFormulaObject.data.checkboxState === this.checkboxState.unchecked) {
                formulaTreeGrid.selectRows(subFormulaObject.recordIndex, null, true);
            } else {
                formulaTreeGrid.selectRows(subFormulaObject.recordIndex, null, false);
            }
            attributeTreeGrid.selectRows(subFormulaObject.recordIndex);
            formulaTreeGrid.cellEdit(subFormulaObject.recordIndex, subFormulaObject.data.item.experimentId);
        }
    }

  onColumnSelection(currentObj) {
    $('body').off('click', '#FormulaTreeGridControl .e-headercell')
      .on('click', '#FormulaTreeGridControl .e-headercell', function (e) {
        currentObj.performSelectAll(e);
        currentObj.setSelectedRowIndex(undefined);
        let headertext;
        const treeObj = $(e.target).closest('.e-gridheader').parent().ejTreeGrid('instance'),
          columnIndex = $(e.target).closest('.e-headercell').index();
        let column, isMovable = false;
        if (treeObj._frozenColumnsLength > 0 && $(e.target).closest('.e-movableheaderdiv').length > 0) {
          column = treeObj.model.columns[columnIndex + treeObj._frozenColumnsLength];
          isMovable = true;
        } else {
          column = treeObj.model.columns[columnIndex];
        }
        treeObj._selectedColumnIndex = columnIndex;
        const rows = treeObj.getRows();
        $(rows[0]).find('.column-selection').removeClass('column-selection');
        $(rows[1]).find('.column-selection').removeClass('column-selection');
        if (column.isFrozen) {
          const columnIndx = $('.e-headercell:contains(' + currentObj.selectedColumnFieldName + ')').closest('.e-headercell').index();
          if (treeObj !== undefined) {
            $(rows[1]).find('td:eq(' + columnIndx + ')').addClass('column-selection');
            $('.e-headercell:contains(' + currentObj.selectedColumnFieldName + ')').addClass('header-back-color');
          }
          return true;
        }
        headertext = column.field;
        currentObj.frozenColumLength = treeObj._frozenColumnsLength;
        if ($(e.target).closest('.e-headercell').hasClass('header-back-color')) {
          // Formula Column deselection
          currentObj.disableCheckboxes();
          currentObj.clearHeaderSelection(treeObj);
          $(rows[isMovable ? 1 : 0]).find('td:eq(' + columnIndex + ')').removeClass('column-selection');
          treeObj._selectedColumnIndex = null;
          currentObj.selectedCol = '';
          currentObj.selectedColumnFieldName = undefined;
          currentObj.allowRowDragDrop = false;
          currentObj.removeAttributeData(currentObj.treeGridData);
          currentObj.refreshAttributeGrid();
        } else {
          currentObj.enableCheckboxes();
          $(rows[isMovable ? 1 : 0]).find('td:eq(' + columnIndex + ')').addClass('column-selection');
          $(e.target).closest('.e-gridheadercontainer').find('.e-headercell').removeClass('header-back-color');
          $(e.target).closest('.e-headercell').addClass('header-back-color');
          currentObj.selectedColIndex = columnIndex;
          currentObj.allowRowDragDrop = true;
          currentObj.selectedColumnFieldName = column.field;
          currentObj.selectedCol = column.field;
          currentObj.calculateAttributeData(currentObj.treeGridData, currentObj.selectedColumnFieldName);
          currentObj.refreshAttributeGrid();
        }
      });
  }
  refreshAttributeGrid() {
    const grid = this.getAttributeTreeGridObject();
    grid.refresh();
  }
  calculateAttributeData(gridData, formula) {
    gridData.forEach(data => {
      let attribute1;
      attribute1 = data[formula];
      if (attribute1 !== undefined) {
        data.attribute1 = attribute1 * 2;
      } else {
        data.attribute1 = '---';
      }
    });
  }

  removeAttributeData(gridData) {
    gridData.forEach(data => {
      if (data.attribute1) {
        delete data.attribute1;
      }
    });
  }

  performSelectAll(e) {
    const treeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    if ($(e.target).hasClass('e-chkbox-wrap') || $(e.target).closest('.e-chkbox-wrap').length > 0) {
      if ($(e.target).hasClass('e-checkmark')) {
        this.isCheckboxSelection = true;
        const formulaIngredients = this.gridData;
        this.clearCheckboxSelection();
        for (const i of Object.keys(formulaIngredients)) {
          const formulaId = this.selectedColumnFieldName + '_id';
          let ingIndex = findIndex(treeObj.model.updatedRecords, { [formulaId]: formulaIngredients[i][formulaId] });
          if (ingIndex === -1) {
            ingIndex = findIndex(treeObj.model.flatRecords, { [formulaId]: formulaIngredients[i][formulaId] });
          }
          if (ingIndex > -1) { treeObj.selectRows(ingIndex, null, true); }
          const index = findIndex(this.treeGridData, { [formulaId]: this.treeGridData[i][formulaId] });
          if (index > -1) {
            const selectedIngredient = this.treeGridData[index];
            this.gridSelectedRows.push(selectedIngredient);
          }
        }
        if (this.gridSelectedRows.length === formulaIngredients.length) {
          $('#FormulaTreeGridControl_headerCheckbox')
            .addClass('e-checkbox e-chk-image e-icon e-headerCheckbox .e-chkbox-wrap .e-chk-image.e-checkmark e-checkmark');
        }
      } else {
        this.isCheckboxSelection = false;
      }
    }
  }

  enableCheckboxes() {
    const formulaTreeGrid = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    formulaTreeGrid.clearSelection();
    const attributeTreeGrid = $('#AttributeTreeGridControl').ejTreeGrid('instance');
    attributeTreeGrid.clearSelection();
    this.clearCheckboxSelection();
    this.disableCheckboxes();
    $('#FormulaTreeGridControl').find('.e-gridheader .e-chkbox-small').removeClass('e-disable').css('pointer-events', 'auto');
    $('#FormulaTreeGridControl').find('.e-treegridrows.gridrowIndexlevel0 .e-chkbox-small').removeClass(
      'e-disable').css('pointer-events', 'auto');
  }

  disableCheckboxes() {
    $('#FormulaTreeGridControl').find('.e-chkbox-small').addClass('e-disable').css('pointer-events', 'none');
    const formulaTreeGrid = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    formulaTreeGrid.clearSelection();
    const attributeTreeGrid = $('#AttributeTreeGridControl').ejTreeGrid('instance');
    attributeTreeGrid.clearSelection();
    this.clearCheckboxSelection();
  }

  clearCheckboxSelection() {
    const formulaTreeGridTreeObj = $('#FormulaTreeGridControl').ejTreeGrid('instance');
    formulaTreeGridTreeObj.selectAllRows(false);
    const attributeTreeGridTreeObj = $('#AttributeTreeGridControl').ejTreeGrid('instance');
    attributeTreeGridTreeObj.selectAllRows(false);
  }

  clearHeaderSelection(treeObj) {
    $('.e-headercell').removeClass('header-back-color');
    $(treeObj.getRows()).find('.column-selection').removeClass('column-selection');
    this.selectedColIndex = -1;
  }

  setBackground(treeObj, selectedColumnIndex) {
    if (!ej.isNullOrUndefined(selectedColumnIndex)) {
      if (treeObj._frozenColumnsLength > 0) {
        $(treeObj.getRows()[1]).find('td:eq(' + selectedColumnIndex + ')').addClass('column-selection');
      } else {
        $(treeObj.getRows()).find('td:eq(' + selectedColumnIndex + ')').addClass('column-selection');
      }
    } else {
      treeObj._selectedColumnIndex = null;
      this.allowRowDragDrop = false;
    }
  }

  public formulaGridContextMenuOpened(args) {
    const formulaTreeGridObj = this.getFormulaTreeGridObject();
    if (args.columnIndex <= 2) {
      args.cancel = true;
    }
    if (args.index >= 0) {
      args.contextMenuItems = [];
      if (formulaTreeGridObj) {
        formulaTreeGridObj._isOnContextMenuAction = true;
      }
      this.showContextMenu(args);
    } else {
      if (this.attributeHeader.length > 0) {
        args.headerContextMenuItems.splice(0);
        this.showContextMenu(args);
      } else {
        args.cancel = true;
      }
    }
  }

  public showContextMenu(args) {
    if (args.contextMenuItems) {
      args.contextMenuItems.splice(0);
      args.contextMenuItems.push.apply(args.contextMenuItems, this.getContextMenuItems(args));
    }
  }
  public getContextMenuItems(args) {
    return [
      {
        headerText: 'View Data',
        isDefault: false,
        disable: false,
        menuId: 'view',
        parentMenuId: null,
        iconClass: 'icon-view-header',
        eventHandler: this.contextmenuClicked.bind(this)
      },
      {
        headerText: 'Open',
        isDefault: false,
        disable: false,
        menuId: 'open',
        parentMenuId: null,
        iconClass: '',
        eventHandler: this.contextmenuClicked.bind(this)
      },
      {
        headerText: 'Create',
        isDefault: false,
        disable: false,
        menuId: 'create',
        parentMenuId: null,
        iconClass: '',
        eventHandler: this.contextmenuClicked.bind(this)
      },
      {
        headerText: 'Delete',
        isDefault: false,
        disable: false,
        menuId: 'delete',
        parentMenuId: null,
        iconClass: '',
        eventHandler: this.contextmenuClicked.bind(this)
      },
      {
        headerText: 'Add',
        isDefault: false,
        disable: false,
        menuId: 'add',
        parentMenuId: null,
        iconClass: '',
        eventHandler: this.contextmenuClicked.bind(this)
      }
    ];
  }

  public contextmenuClicked(args) {
    if (args.menuId === 'view') {
      console.log(args);
    }
  }
  private setSelectedRowIndex(recordIndex) {
    this.selectedRowIndex = recordIndex;
  }

  getPreviousZeroLevelRowIndex() {
    const formulaTreeGridWidget = this.getFormulaTreeGridObject();
    const updatedRecords = formulaTreeGridWidget.model.updatedRecords;
    let rowIndex = this.selectedRowIndex;
    for (let i = this.selectedRowIndex - 1; i >= 0; i--) {
      if (updatedRecords[i].level === 0) {
        rowIndex = i;
        break;
      }
    }
    return rowIndex;
  }
  getNextZeroLevelRowIndex() {
    const formulaTreeGridWidget = this.getFormulaTreeGridObject();
    const updatedRecords = formulaTreeGridWidget.model.updatedRecords;
    let rowIndex = this.selectedRowIndex;
    for (let i = this.selectedRowIndex + 1; i < updatedRecords.length; i++) {
      if (updatedRecords[i].level === 0) {
        rowIndex = i;
        break;
      }
    }
    return rowIndex;
  }

  upArrow(e) {
    const formulaTreeGrid = this.getFormulaTreeGridObject();
    if (formulaTreeGrid.model.isEdit) {
      const highlightRowIndex = this.getPreviousZeroLevelRowIndex();
      formulaTreeGrid.setModel({ selectedRowIndex: highlightRowIndex });
      formulaTreeGrid.updateScrollBar();
      formulaTreeGrid.cellEdit(highlightRowIndex, formulaTreeGrid._cellEditingDetails.fieldName);
      this.isEndEditCalled = false;
    }
  }

  downArrow() {
    const formulaTreeGrid = this.getFormulaTreeGridObject();
    if (formulaTreeGrid.model.isEdit) {
      let highlightRowIndex;
      if (this.isSubformulaSelected) {
        highlightRowIndex = this.getNextZeroLevelRowIndex();
      } else {
        highlightRowIndex = this.selectedRowIndex + 1;
      }
            this.selectedRowIndex = highlightRowIndex;
      formulaTreeGrid.setModel({ selectedRowIndex: highlightRowIndex });
      formulaTreeGrid.updateScrollBar();
      formulaTreeGrid.cellEdit(highlightRowIndex, formulaTreeGrid._cellEditingDetails.fieldName);
      this.isEndEditCalled = false;
    }
  }
  pageUp(e) {
    const treeGridObj = this.getFormulaTreeGridObject();
    if (treeGridObj.model.isEdit) {
      treeGridObj.setModel({ selectedRowIndex: 0 });
      treeGridObj.updateScrollBar();
      treeGridObj.cellEdit(treeGridObj.selectedRowIndex(), treeGridObj._cellEditingDetails.fieldName);
    }
  }
  pageDown(e) {
    const treeGridObj = this.getFormulaTreeGridObject();
    if (treeGridObj.model.isEdit) {
      treeGridObj.setModel({ selectedRowIndex: treeGridObj.model.updatedRecords.length - 1 });
      treeGridObj.updateScrollBar();
      treeGridObj.cellEdit(treeGridObj.selectedRowIndex(), treeGridObj._cellEditingDetails.fieldName);
    }
  }

  public columnDragStart(args) {
    if ((args.draggedColumn.field === 'taskID') || (args.draggedColumn.field === 'taskName')) {
      args.cancel = true;
    }
  }

  public columnDrop(args) {
    if ((args.targetColumn.field === 'taskID') ||
      (args.targetColumn.field === 'taskName') ||
      (args.targetColumn.field === 'iconReference') ||
      (args.targetColumn.field === 'warningIconReference')) {
      args.cancel = true;
    } else {
      this.columnDrag = [];
      this.columnDrag.push({ 'field': args.draggedColumn.field, 'index': args.draggedColumnIndex, 'type': 'columnReorder' });
    }
    const formulaTreeGridWidget = this.getFormulaTreeGridObject();
    this.clearBackground(formulaTreeGridWidget);
    if (args.draggedColumnIndex <= 2) {
      args.cancel = true;
      return args;
    }
    setTimeout(() => {
      if (this.selectedColIndex !== -1) {
        this.addColumnSelectionClasses(this.getFormulaTreeGridObject(), args.draggedColumn.field, true);
      }
    }, 10);
  }

  public columnDragStartAttribute(args) {
    if ((args.draggedColumn.field === 'ingredientName') || (args.draggedColumn.field === 'ingredientNumber')) {
      args.cancel = true;
    }
  }

  public columnDropAttribute(args) {
    if ((args.targetColumn.field === 'taskName') ||
      (args.targetColumn.field === 'taskID') ||
      (args.targetColumn.field === 'iconReference')) {
      args.cancel = true;
    } else {
      this.columnDrag = [];
      this.columnDrag.push({ 'field': args.draggedColumn.field, 'index': args.draggedColumnIndex, 'type': 'columnReorder' });
    }
    const formulaTreeGridWidget = this.getFormulaTreeGridObject();
    this.clearBackground(formulaTreeGridWidget);
  }

  addColumnSelectionClasses(treeObj, experimentId, isRemoveColumnSelection) {
    let columnIndex = findIndex(treeObj.model.columns, { field: experimentId });
    columnIndex = columnIndex - treeObj.model.scrollSettings.frozenColumns;
    const table = $(document.getElementById('e-movableheaderdivFormulaTreeGridControl'))[0].children[0].children[1];
    $(table).find('th:eq(' + columnIndex + ')').addClass('header-back-color');
    const rows = treeObj.getRows();
    if (isRemoveColumnSelection) {
      $('.column-selection').removeClass('column-selection');
    }
    $(rows[1]).find('td:eq(' + columnIndex + ')').addClass('column-selection');
    this.selectedColIndex = columnIndex;
    treeObj._selectedColumnIndex = columnIndex;
  }

  public clearBackground(treeObj) {
    if (!ej.isNullOrUndefined(treeObj._selectedColumnIndex)) {
      if (treeObj._frozenColumnsLength > 0) {
        $(treeObj.getRows()[1]).find('.column-selection').removeClass('column-selection');
      } else {
        $(treeObj.getRows()).find('.column-selection').removeClass('column-selection');
      }
      treeObj._selectedColumnIndex = null;
    }
  }

  columnResizeStarted(args) {
    if (args.columnIndex <= 2) {
      args.cancel = true;
    }
  }

  columnResized(args) {
    if (this.formulaHeader.length < 5) {
      this.updateSpliterPositionOnColumnResize();
    }
  }

  columnResizedAttribute(args) {
    if (this.formulaHeader.length < 5) {
      this.updateSpliterPositionOnColumnResize();
    }
  }

  addNewRecords() {
    this.treeGridData.push({
      taskID: 40, taskName: 'Planning new', startDate: '02/03/2014', endDate: '02/07/2014', progress: 100, duration: 5,
      Formula1: 0, Formula1_id: 201,
    });
  }

  addNewColumn() {
    this.formulaHeader.push({ 'text': 'Formula6' });
    for (let i = 0; i < this.treeGridData.length; i++) {
      this.treeGridData[i].Formula6 = '-xxx-';
    }
    this.gridData = this.treeGridData;
  }

  sortColumn(args) {
    this.treeGridData.reverse();
    this.refreshGrid();
  }
  refreshGrid() {
    const grid = this.getFormulaTreeGridObject();
    grid.refresh();
  }
  public formulaGridRowDragStarted(args) {
    if (args.draggedRow.level > 0) {
      args.canDrop = false;
      args.cancel = true;
    }
  }

  public rowDragEventTriggered(args) {
    const treeGrid = this.getFormulaTreeGridObject();
    const rows = treeGrid.getRows();
    $('.above-ingredient').removeClass('above-ingredient');
    $('.below-ingredient').removeClass('below-ingredient');
    $('.e-aboveIcon').removeClass('e-aboveIcon');
        $('.e-belowIcon').removeClass('e-rowcell e-belowIcon');
    $('.e-cancelIcon').removeClass('e-cancelIcon');
    if ((args.draggedRow.level > 0 || (args.targetRow !== null && args.targetRow.level > 0)) || args.dropPosition === 'insertAsChild') {
      args.canDrop = false;
      if (args.dropPosition !== 'insertAsChild') {
        args.cancel = true;
      }
    } else {
      if (args.dropPosition === 'insertAbove') {
        $(rows[0][args.targetRowIndex]).addClass('above-ingredient');
        $(rows[1][args.targetRowIndex]).addClass('above-ingredient');
      } else if (args.dropPosition === 'insertBelow') {
        $(rows[0][args.targetRowIndex]).addClass('below-ingredient');
        $(rows[1][args.targetRowIndex]).addClass('below-ingredient');
      }
    }
  }
  formulaRowDragStopped(args) {
    $('.above-ingredient').removeClass('above-ingredient');
    $('.below-ingredient').removeClass('below-ingredient');
    this.treeGridData = args.model.dataSource().slice();
    const tempGridData = cloneDeep(this.treeGridData);
    this.treeGridData = tempGridData.slice();
    this.dataSourceFlagFormula = true;
    this.dataSourceFlagAttr = true;
  }

  public attributeGridRowDragStarted(args) {
    args.cancel = true;
  }

  public attributeRowDragStopped(args) {
    this.gridData = args.model.dataSource().slice();
  }

  public ingredientDragcheck(dragItemData) {
    const formula = this.selectedColumnFieldName;
    if (dragItemData.draggedRow.index <= this.treeGridData.length && dragItemData.draggedRow[formula] !== undefined) {
      return true;
    } else {
      return false;
    }
  }

  sortGrid(formula, order) {
    event.stopPropagation();
    const formulaTreeGrid = this.getFormulaTreeGridObject();
    const attributeTreeGrid = this.getAttributeTreeGridObject();
    if (formula === 'none') {
      formulaTreeGrid.clearSorting();
      attributeTreeGrid.clearSorting();
            for (let i = 0, length = this.formulaHeader.length; i < length; i++) {
                this.formulaHeader[i].sortOrder = ej.sortOrder.Ascending;
            }
    } else {
      formulaTreeGrid.sortColumn(formula, order);
      attributeTreeGrid.sortColumn(formula, order);
            if ((formula !== 'taskName') && (formula !== 'taskID')) {
                const index = this.formulaHeader.length ?
                    findIndex(this.formulaHeader, { 'formula': formula }) : -1;
                if (order === ej.sortOrder.Ascending) {
                    this.formulaHeader[index].sortOrder = ej.sortOrder.Descending;
                } else if (order === ej.sortOrder.Descending) {
                    this.formulaHeader[index].sortOrder = ej.sortOrder.Ascending;
                }
            }
            this.treeGridData = formulaTreeGrid._sortedRecords;
    }
  }

  showSummary() {
    this.summaryRowsForAttributeGrid = this.getSummaryRowsForAttributeGrid();
  }

  getSummaryRowsForAttributeGrid() {
    return [{
      title: '',
      calculationType: 'total',
      summaryColumns: [
        {
          summaryType: null,
          displayColumn: 'taskID',
          prefix: 'Sum',
          format: '{}'
        },
        {
            summaryType: ej.TreeGrid.SummaryType.Count,
            displayColumn: 'Formula1',
            dataMember: 'Formula1',
            prefix: ''
        }]
    },
    {
      title: '',
      calculationType: 'sum',
      summaryColumns: [{
        summaryType: null,
        displayColumn: 'taskID',
        prefix: 'Sum',
        format: '{}'
      }]
    },
    {
      title: '',
      calculationType: '',
      summaryColumns: [
        {
          summaryType: null,
          displayColumn: 'taskID',
          prefix: 'Sum',
          format: '{}'
        }]
    },
    {
      title: '',
      calculationType: '',
      summaryColumns: [
        {
          summaryType: null,
          displayColumn: 'taskID',
          prefix: 'Sum',
          format: '{}'
        }]
    }
    ];
  }

  colorDifferentSFLevels(event) {
    const colorLevel = event.data.level;
    if (event.column.field !== 'iconReference' &&
      event.column.field !== 'warningIconReference') {
      if (event.data.item.subformula !== undefined && event.data.expanded === true) {
        $(event.cellElement).css({ 'background': this.colorList[colorLevel].primarycolor });
      }
      if (event.data.level > 0) {
        if (event.data.item.subformula !== undefined) {
          if (event.column.field === 'taskName') {
            let levelIndent = 0;
            let colorWithStyle = 'linear-gradient(90deg, ';
            for (let i = 0; i <= colorLevel; i++) {
              if (i === colorLevel) {
                colorWithStyle = colorWithStyle + this.colorList[i].primarycolor + ' ' + levelIndent + 'px';
              } else {
                colorWithStyle = colorWithStyle + this.colorList[i].primarycolor + ' ' + levelIndent + 'px,';
                levelIndent += 12;
                colorWithStyle = colorWithStyle + this.colorList[i].primarycolor + ' ' + levelIndent + 'px,';
              }
            }
            colorWithStyle = colorWithStyle + ')';
            $(event.cellElement).css({ 'background': colorWithStyle });
          } else {
            $(event.cellElement).css({ 'background': this.colorList[colorLevel].primarycolor });
          }
        } else {
          if (event.column.field === 'taskName') {
            let levelIndent = 0;
            let colorWithStyle = 'linear-gradient(90deg, ';
            for (let i = 0; i <= colorLevel; i++) {
              if (i === colorLevel) {
                colorWithStyle = colorWithStyle + this.colorList[i - 1].secondarycolor + ' ' + levelIndent + 'px';
              } else {
                colorWithStyle = colorWithStyle + this.colorList[i].primarycolor + ' ' + levelIndent + 'px,';
                levelIndent += 12;
                colorWithStyle = colorWithStyle + this.colorList[i].primarycolor + ' ' + levelIndent + 'px,';
              }
            }
            colorWithStyle = colorWithStyle + ')';
            $(event.cellElement).css({ 'background': colorWithStyle });
          } else {
            $(event.cellElement).css({ 'background': this.colorList[colorLevel - 1].secondarycolor });
          }
        }
      }
    }
  }

  // this is where dynamically inserting the data and it refreshes the grid and insert the sub formula item. But
  // this will not happen when the cell editing is in focus
  insert() {
    this.dataSourceFlagAttr = true;
    this.dataSourceFlagFormula = true;
    this.treeGridData = this.treeGridData1;
  }
}
