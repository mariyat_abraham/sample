import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    isWorkspace: boolean;
    isDashboard: boolean;
    activeIndex: any;

    ngOnInit() {
        this.isDashboard = true;
    }

    tabSelection(args) {
        if (args.activeIndex > 0 ) {
            this.isWorkspace = true;
            this.isDashboard = false;
            this.activeIndex = args.activeIndex;
        } else if (args.activeIndex === 0 ) {
            this.isWorkspace = false;
            this.isDashboard = true;
        }
    }
}
